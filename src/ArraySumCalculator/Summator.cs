﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ArraySumCalculator
{
    public class Summator
    {
        private readonly int _length;
        private Random _random;
        private int[] _array;
        private Stopwatch _sp;
        private int _threadCount;

        public int SumResult
        {
            get => Sum();
        }
        public long DefaultSumTime
        {
            get
            {
                _sp.Reset();
                _sp.Start();
                Sum();
                _sp.Stop();
                return _sp.ElapsedMilliseconds;
            }
        }
        public long ThreadSumTime
        {
            get
            {
                _sp.Reset();
                var chanks = GetChanks();
                _sp.Start();
                SumThread(chanks, new Task<int>[_threadCount]);
                _sp.Stop();
                return _sp.ElapsedMilliseconds;
            }
        }
        public long PLinqSumTime
        {
            get
            {
                _sp.Reset();
                _sp.Start();
                SumPlinq();
                _sp.Stop();
                return _sp.ElapsedMilliseconds;
            }
        }

        public Summator(int length)
        {
            _length = length;
            _random = new Random();
            _sp = new Stopwatch();
            _array = new int[_length];
            for (int i = 0; i < _length; i++)
            {
                _array[i] = _random.Next(-100, 100);
            }
            _threadCount = 4;
        }

        private int Sum() => _array.Sum();

        private IEnumerable<int[]> GetChanks()
        {
            return _array.Chunk(_array.Length / _threadCount);
        }

        private int SumThread(IEnumerable<int[]> chanks, Task<int>[] taskArray)
        {
            int i = 0;
            foreach(var chank in chanks)
            {
                taskArray[i] = Task.Run(() => chank.Sum());
                i++;
            }
            Task.WaitAll(taskArray);
            return taskArray.Sum(t => t.Result);
        }

        private int SumPlinq() => _array.AsParallel().Sum();
    }
}
