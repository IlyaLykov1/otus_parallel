﻿using ArraySumCalculator;
using System;

namespace ConsoleUI
{
    public class Program
    {
        static void Main(string[] args)
        {
            var summatorShort = new Summator(100_000);
            Console.WriteLine($"100_000: TotSum: {summatorShort.SumResult}, " +
                $"DefaultTime: {summatorShort.DefaultSumTime}, " +
                $"ThreadTime: {summatorShort.ThreadSumTime}, " +
                $"PLINQTime: {summatorShort.PLinqSumTime}");

            var summatorMiddle = new Summator(1_000_000);
            Console.WriteLine($"1_000_000: TotSum: {summatorMiddle.SumResult}, " +
                $"DefaultTime: {summatorMiddle.DefaultSumTime}, " +
                $"ThreadTime: {summatorMiddle.ThreadSumTime}, " +
                $"PLINQTime: {summatorMiddle.PLinqSumTime}");

            var summatorLong = new Summator(10_000_000);
            Console.WriteLine($"10_000_000: TotSum: {summatorLong.SumResult}, " +
                $"DefaultTime: {summatorLong.DefaultSumTime}, " +
                $"ThreadTime: {summatorLong.ThreadSumTime}, " +
                $"PLINQTime: {summatorLong.PLinqSumTime}");
        }
    }
}
